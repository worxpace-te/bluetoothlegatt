/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hmsoft.bluetooth.le;

import com.example.bluetooth.le.R;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import static android.widget.TextView.OnEditorActionListener;


/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

	public static final String COMMAND_STOP = "COMMAND_STOP";
	public static final String COMMAND_PAUSE = "COMMAND_PAUSE";
	public static final String COMMAND_RESUME = "COMMAND_RESUSE";
	public static final String COMMAND_FUP = "COMMAND_FUP";
	public static final String COMMAND_FDOWN = "COMMAND_FDOWN";
	public static final String COMMAND_STATUS = "COMMAND_STATUS";
	public static final String COMMAND_VERSION = "COMMAND_VERSION";
	public static final String COMMAND_F1 = "COMMAND_F1";
	public static final String COMMAND_F2 = "COMMAND_F2";

	private OnEditorActionListener m_OnEditorActionListener;

    private TextView mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private BluetoothLeService mBluetoothLeService;
    private boolean mConnected = false;
    
    EditText edtSend;
	ScrollView svResult;
	Button btnSend;

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            
            Log.e(TAG, "mBluetoothLeService is okay");
            // Automatically connects to the device upon successful start-up initialization.
            //mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
            	Log.e(TAG, "Only gatt, just wait");
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                invalidateOptionsMenu();
                btnSend.setEnabled(false);
                clearUI();
            }else if(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action))
            {
            	mConnected = true;
            	mDataField.setText("");
            	ShowDialog();
            	btnSend.setEnabled(true);
            	Log.e(TAG, "In what we need");
            	invalidateOptionsMenu();
            }else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
            	Log.e(TAG, "RECV DATA");
            	String data = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);
            	if (data != null) {
                	if (mDataField.length() > 500)
                		mDataField.setText("");
                    mDataField.append(data); 
                    svResult.post(new Runnable() {
            			public void run() {
            				svResult.fullScroll(ScrollView.FOCUS_DOWN);
            			}
            		});
                }
            }
        }
    };

    private void clearUI() {
        mDataField.setText(R.string.no_data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        mDataField = (TextView) findViewById(R.id.data_value);
        edtSend = (EditText) this.findViewById(R.id.edtSend);
        edtSend.setText("BLETest\\r\\n");
        svResult = (ScrollView) this.findViewById(R.id.svResult);
        
        btnSend = (Button) this.findViewById(R.id.btnSend);
		btnSend.setOnClickListener(new ClickEvent());
		btnSend.setEnabled(false);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        Log.d(TAG, "Try to bindService=" + bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE));
	    final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(DeviceControlActivity.this);
	    m_OnEditorActionListener = new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
			    if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
				    // Write to preferences
				    String command = null;
				    switch (textView.getId()) {
					    case R.id.editCommandStop:
						    command = COMMAND_STOP;
						    break;
					    case R.id.editCommandPause:
						    command = COMMAND_PAUSE;
						    break;
					    case R.id.editCommandResume:
						    command = COMMAND_RESUME;
						    break;
					    case R.id.editCommandFup:
						    command = COMMAND_FUP;
						    break;
					    case R.id.editCommandFdown:
						    command = COMMAND_FDOWN;
						    break;
					    case R.id.editCommandStatus:
						    command = COMMAND_STATUS;
						    break;
					    case R.id.editCommandVersion:
						    command = COMMAND_VERSION;
						    break;
					    case R.id.editCommandF1:
						    command = COMMAND_F1;
						    break;
					    case R.id.editCommandF2:
						    command = COMMAND_F2;
						    break;
				    }
				    if(command != null) {
					    SharedPreferences.Editor editor = preferences.edit();
					    editor.putString(command, textView.getText().toString());
					    editor.commit();
				    }
				    return false; // consume.
			    }
			    return false; // pass on to other listeners.
		    }
	    };

	    String stopString = preferences.getString(COMMAND_STOP, "=>HVSTOP\\r\\n");
	    String pauseString = preferences.getString(COMMAND_PAUSE, "=>HVPAUSE\\r\\n");
	    String resumeString = preferences.getString(COMMAND_RESUME, "=>HVRESUME\\r\\n");
	    String fupString = preferences.getString(COMMAND_FUP, "=>FUP\\r\\n");
	    String fdownString = preferences.getString(COMMAND_FDOWN, "=>FDOWN\\r\\n");
	    String statusString = preferences.getString(COMMAND_STATUS, "=>HVSTAT\\r\\n");
	    String versionString = preferences.getString(COMMAND_VERSION, "=>HVVER\\r\\n");
	    String f1String = preferences.getString(COMMAND_F1, "=>AUP\\r\\n");
	    String f2String = preferences.getString(COMMAND_F2, "=>ADOWN\\r\\n");

	    final EditText stopEdit = (EditText) this.findViewById(R.id.editCommandStop);
	    stopEdit.setText(stopString);
	    stopEdit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button stopButton = (Button) this.findViewById(R.id.btnSendCommandStop);
	    stopButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(stopEdit);
		    }
	    });

	    final EditText pauseEdit = (EditText) this.findViewById(R.id.editCommandPause);
	    pauseEdit.setText(pauseString);
	    pauseEdit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button pauseButton = (Button) this.findViewById(R.id.btnSendCommandPause);
	    pauseButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(pauseEdit);
		    }
	    });

	    final EditText resumeEdit = (EditText) this.findViewById(R.id.editCommandResume);
	    resumeEdit.setText(resumeString);
	    resumeEdit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button resumeButton = (Button) this.findViewById(R.id.btnSendCommandResume);
	    resumeButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(resumeEdit);
		    }
	    });

	    final EditText fupEdit = (EditText) this.findViewById(R.id.editCommandFup);
	    fupEdit.setText(fupString);
	    fupEdit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button fupButton = (Button) this.findViewById(R.id.btnSendCommandFup);
	    fupButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(fupEdit);
		    }
	    });

	    final EditText fdownEdit = (EditText) this.findViewById(R.id.editCommandFdown);
	    fdownEdit.setText(fdownString);
	    fdownEdit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button fdownButton = (Button) this.findViewById(R.id.btnSendCommandFdown);
	    fdownButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(fdownEdit);
		    }
	    });

	    final EditText statusEdit = (EditText) this.findViewById(R.id.editCommandStatus);
	    statusEdit.setText(statusString);
	    statusEdit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button statusButton = (Button) this.findViewById(R.id.btnSendCommandStatus);
	    statusButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(statusEdit);
		    }
	    });

	    final EditText versionEdit = (EditText) this.findViewById(R.id.editCommandVersion);
	    versionEdit.setText(versionString);
	    versionEdit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button versionButton = (Button) this.findViewById(R.id.btnSendCommandVersion);
	    versionButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(versionEdit);
		    }
	    });

	    final EditText f1Edit = (EditText)this.findViewById(R.id.editCommandF1);
	    f1Edit.setText(f1String);
	    f1Edit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button f1Button = (Button) this.findViewById(R.id.btnSendCommandF1);
	    f1Button.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(f1Edit);
		    }
	    });

	    final EditText f2Edit = (EditText)this.findViewById(R.id.editCommandF2);
	    f2Edit.setText(f2String);
	    f2Edit.setOnEditorActionListener(m_OnEditorActionListener);

	    Button f2Button = (Button) this.findViewById(R.id.btnSendCommandF2);
	    f2Button.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    HandleButtonClick(f2Edit);
		    }
	    });

	    Button resetButton = (Button) this.findViewById(R.id.btnRestoreDefaults);
	    resetButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    SharedPreferences.Editor editor = preferences.edit();
			    editor.putString(COMMAND_STOP, "=>HVSTOP\\r\\n");
			    editor.putString(COMMAND_PAUSE, "=>HVPAUSE\\r\\n");
			    editor.putString(COMMAND_RESUME, "=>HVRESUME\\r\\n");
			    editor.putString(COMMAND_FUP, "=>FUP\\r\\n");
			    editor.putString(COMMAND_FDOWN, "=>FDOWN\\r\\n");
			    editor.putString(COMMAND_STATUS, "=>HVSTAT\\r\\n");
			    editor.putString(COMMAND_VERSION, "=>HVVER\\r\\n");
			    editor.putString(COMMAND_F1, "=>AUP\\r\\n");
			    editor.putString(COMMAND_F2, "=>ADOWN\\r\\n");
			    editor.commit();

			    stopEdit.setText("=>HVSTOP\\r\\n");
			    pauseEdit.setText("=>HVPAUSE\\r\\n");
			    resumeEdit.setText("=>HVRESUME\\r\\n");
			    fupEdit.setText("=>FUP\\r\\n");
			    fdownEdit.setText("=>FDOWN\\r\\n");
			    statusEdit.setText("=>HVSTAT\\r\\n");
			    versionEdit.setText("=>HVVER\\r\\n");
			    f1Edit.setText("=>AUP\\r\\n");
			    f2Edit.setText("=>ADOWN\\r\\n");
		    }
	    });

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

	private void HandleButtonClick(EditText textToSend)
	{
		if(!mConnected) return;

		if (textToSend.length() < 1) {
			Toast.makeText(DeviceControlActivity.this, "No characters to send", Toast.LENGTH_SHORT).show();
			return;
		}
		String toSend = textToSend.getText().toString();
		toSend = toSend.replace("\\n", "\n");
		toSend = toSend.replace("\\r", "\r");
		mBluetoothLeService.WriteValue(toSend);

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm.isActive())
			imm.hideSoftInputFromWindow(textToSend.getWindowToken(), 0);
	}

    @Override
    protected void onResume() {
        super.onResume();
        /*registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
        unbindService(mServiceConnection);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //this.unregisterReceiver(mGattUpdateReceiver);
        //unbindService(mServiceConnection);
        if(mBluetoothLeService != null)
        {
        	mBluetoothLeService.close();
        	mBluetoothLeService = null;
        }
        Log.d(TAG, "We are in destroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
            	if(mConnected)
            	{
            		mBluetoothLeService.disconnect();
            		mConnected = false;
            	}
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private void ShowDialog()
    {
    	Toast.makeText(this, "Connected to Device", Toast.LENGTH_SHORT).show();
    }

	class ClickEvent implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			if (v == btnSend) {
				if(!mConnected) return;
				
				if (edtSend.length() < 1) {
					Toast.makeText(DeviceControlActivity.this, "No characters to send", Toast.LENGTH_SHORT).show();
					return;
				}
				String toSend = edtSend.getText().toString();
				toSend = toSend.replace("\\n", "\n");
				toSend = toSend.replace("\\r", "\r");
				mBluetoothLeService.WriteValue(toSend);
				
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				if(imm.isActive())
					imm.hideSoftInputFromWindow(edtSend.getWindowToken(), 0);
				//todo Send data
			}
		}
	}

	private void InitializeTextFields()
	{
		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);




	}
	
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothDevice.ACTION_UUID);
        return intentFilter;
    }
}
